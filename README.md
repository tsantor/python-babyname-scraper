# Python Baby Name Scraper

## Overview
Script for scraping data on baby names from the Social Security Administration website.

## Usage
```
python babynames.py -s 1979 -e 2019 --output=csv
```

> **NOTE:** For all usage options run `python babaynames -h`.

## Resources
- [Social Security Administration - Baby Names](https://www.ssa.gov/oact/babynames/)
- [Python Baby Names Exercise](https://developers.google.com/edu/python/exercises/baby-names)
